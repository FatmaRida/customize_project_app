# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'ps_gantt',
    'version': '14.0.2.2.0',
    'category': 'tasks',
    'summary': 'tasks',
    'sequence': '10',
    'author': 'eman',
    'license': 'LGPL-3',
    'company': 'eman',
    'maintainer': '',
    'support': 'eman@gmail.com',
    'website': 'https://www.odoomates.tech',
    'depends': ['base','project'],
    'live_test_url': 'https://www.youtube.com/watch?v=Qu6R3yNKR60',
    'demo': [],
    'data': [
       'views/ga.xml'
       ,
       'views/customize_wizard_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
    'images': ['static/description/banner.gif'],
    'qweb': [],
}
