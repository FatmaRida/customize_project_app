# -*- coding: utf-8 -*-

from odoo import models, fields,api,_


class CustomizeProject(models.Model):
    _inherit = 'project.project'

    name = fields.Char('Project Name',required=True)
    allow_timesheets = fields.Boolean('Timesheets',default=True)

    allow_recurring_tasks = fields.Boolean('Recurring Tasks',default=True)


