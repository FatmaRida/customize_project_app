from odoo import api, fields, models


class ProjectTaskInherit(models.Model):
    _inherit = 'project.task'

    # assigned_id = fields.Many2many('res.users',"Assigned resources")
    precent_done = fields.Integer("Done%")
    duration = fields.Integer("Duration (days)" , readonly=True)
    effort = fields.Integer("Effort (hours)")
    scheduling_mode= fields.Selection([('Normal', 'Normal'), ('FixedDuration', 'FixedDuration') , ('FixedEffort', 'FixedEffort'),('FixedUnits', 'FixedUnits')], string='Scheduling Mode')
    constraint_type= fields.Selection([('muststarton', 'Must start on'), ('mustfinishon', 'Must finish on') , ('startnoearlierthan', 'Start no earlier than'),('startnolaterthan', 'Start no later than'),('finishnoearlierthan', 'Finish no earlier than'),('finishnolaterthan', 'finish no later than')], string='Constraint Type')
    constraint_date = fields.Datetime('Constraint Date')
    effort_driven = fields.Boolean("Effort Driven")
    manually_scheduled = fields.Boolean("Manually Scheduled")
    effected_hours = fields.Float('Hours Spent' , readonly=True)
    progress = fields.Float('Progress' , readonly=True)